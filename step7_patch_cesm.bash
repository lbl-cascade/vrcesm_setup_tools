#!/bin/bash

source common_env.bash
module load cray-netcdf

export VRCESMBASE=$PWD

# calculate the NE level of the highest resolution
export FINE_NE_LEVEL=`python -c "print(int(${COARSE_NE_LEVEL} * 2**(${NUM_REFINEMENT}-1)))"`

# get the grid size from the SCRIP file
export NCOL=`ncdump -h scrip/${DOMAIN}.g_scrip.nc |grep "grid_size = " | sed 's/.*grid_size\ =\ \(.*\)\ ;/\1/g'`

# set the size of the original grid from which topography was generated (TODO: set this programmatically)
export NCUBE=3000

# generate the patch
PATCHFILE=$PWD/cesm_patch.${DOMAIN}.diff
cat cesm_patch.perl_files.diff.template > ${PATCHFILE}
cat cesm_patch.diff.template | envsubst >> ${PATCHFILE}

pushd cesm1_2_2
patch -p0 -i ${PATCHFILE}
popd
