#!/bin/bash

source common_env.bash
module load tempestremap

WORKDIR=$PWD/regrid/scrip
mkdir -p $WORKDIR

SOURCE_MESH=$PWD/grid/${DOMAIN}.g
TARGET_MESH_HIGH=$WORKDIR/0.25degree.g
OVERLAP_MESH_HIGH=$WORKDIR/overlap_0.25degree_${DOMAIN}.g
WEIGHT_FILE_HIGH=${WORKDIR}/weights_0.25degree_${DOMAIN}_nonmonotonic.g
TARGET_MESH_LOW=$WORKDIR/1degree.g
OVERLAP_MESH_LOW=$WORKDIR/overlap_1degree_${DOMAIN}.g
WEIGHT_FILE_LOW=${WORKDIR}/weights_1degree_${DOMAIN}_nonmonotonic.g
REGENERATE=0

pushd $WORKDIR

# generate the mesh file for the high-res target grid
if [ ! -e "${TARGET_MESH_HIGH}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateRLLMesh \
        --lat 720 \
        --lon 1440 \
        --file $TARGET_MESH_HIGH
fi

# generate the mesh file for the low-res target grid
if [ ! -e "${TARGET_MESH_LOW}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateRLLMesh \
        --lat 180 \
        --lon 360 \
        --file $TARGET_MESH_LOW
fi


# generate the high-res overlap mesh
if [ ! -e "${OVERLAP_MESH_HIGH}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateOverlapMesh \
        --a ${SOURCE_MESH} \
        --b ${TARGET_MESH_HIGH} \
        --out ${OVERLAP_MESH_HIGH}
fi

# generate the low-res overlap mesh
if [ ! -e "${OVERLAP_MESH_LOW}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateOverlapMesh \
        --a ${SOURCE_MESH} \
        --b ${TARGET_MESH_LOW} \
        --out ${OVERLAP_MESH_LOW}
fi

# generate high-res regridding weights
if [ ! -e "${WEIGHT_FILE_HIGH}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateOfflineMap \
        --in_mesh ${SOURCE_MESH} \
        --out_mesh ${TARGET_MESH_HIGH} \
        --ov_mesh ${OVERLAP_MESH_HIGH} \
        --in_type cgll \
        --out_type fv \
        --in_np 4 \
        --out_map ${WEIGHT_FILE_HIGH}
fi

# generate low-res regridding weights
if [ ! -e "${WEIGHT_FILE_LOW}" -o "${REGENERATE}" -eq "1" ]; then
    GenerateOfflineMap \
        --in_mesh ${SOURCE_MESH} \
        --out_mesh ${TARGET_MESH_LOW} \
        --ov_mesh ${OVERLAP_MESH_LOW} \
        --in_type cgll \
        --out_type fv \
        --in_np 4 \
        --out_map ${WEIGHT_FILE_LOW}
fi

# link the regridding weights to the top-level regridding directory
ln -sf $WEIGHT_FILE_HIGH ../
ln -sf $WEIGHT_FILE_LOW ../

popd
