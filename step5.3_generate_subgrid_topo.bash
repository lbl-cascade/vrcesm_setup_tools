#!/bin/bash

source common_env.bash

module load ucar_topo
WORKDIR=$PWD/topo/subgrid/
UCARTOPO=$WORKDIR/UCAR_Topo/

# copy the UCAR topo tool
# TODO: consider symlinkng all pieces to avoid large space requirement
echo "Syncing $UCAR_TOPO_BASE with $UCARTOPO"
mkdir -p $UCARTOPO
rsync -a $UCAR_TOPO_BASE/ $UCARTOPO

TARGET_SCRIP=$PWD/scrip/${DOMAIN}.g_scrip.nc
SMOOTHED_TOPO_FILE=$PWD/topo/smoothed/${DOMAIN}_topo_smoothed.nc

# link files
ln -sf $TARGET_SCRIP $UCARTOPO/cube_to_target/inputdata/grid-descriptor-file/input_scrip.nc
ln -sf $SMOOTHED_TOPO_FILE $UCARTOPO/cube_to_target/inputdata/externally-smoothed-PHIS/input_topo.nc

pushd $UCARTOPO
make 
echo "Done."

popd

# link the final file to the topo directory
ln -sf $UCARTOPO/cube_to_target/output/se_${DOMAIN}-gtopo30-cam_se_smooth-intermediate_ncube3000-no_anisoSGH.nc $PWD/topo
