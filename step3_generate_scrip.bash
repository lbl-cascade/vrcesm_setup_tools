#!/bin/bash
#SBATCH -N 1
#SBATCH -C haswell
#SBATCH -p regular
#SBATCH -t 00:30:00

source common_env.bash

export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

module load ncl
module load openmpi
module load nco
module load homme

########################################################

meshdir=$PWD/grid/
src=$HOMME/build/src/preqx
wdir=$PWD/scrip
wdir_main=$PWD/scrip

########################################################


#
# Mark Taylor 2010
#
# generate template  files used for SCRIP/ESMF and interpic, and paraview metadata files
# HOMME is used to run a short (1 timestep) dummy simulation.  the output is then
# processed to NCO & NCL to produce:
#   
# for running CAM physics on GLL points:  (colocated)
#   ne$NEnp$NP_latlon.nc     template for interpic_new and paraview metadata
#   ne$NEnp$NP_scrip.nc      template for SCRIP/ESMF mapping utility
#
# for running CAM physics on CSLAM grid
#   ne$NEnc$NP_latlon.nc    template for interpic_new and paraview metadata
#   ne$NEnc$NP_scrip.nc     template for SCRIP/ESMF mapping utility
#                                  
#
#
# NE=240 runs in a few minutes.  needs 24 nodes, io_stride=8
#
# to plot the control volumes, see:   plotgrid.ncl
#

echo "setting up GLL/CSLAM pts"
# GLL points within each element:
NPTS=4
# CSLAM points points within each element:
NC=4  # must match value in dimensions_mod.F90

echo "setting up unstructured grid from file"
ne=0

meshname=$DOMAIN.g
meshfile=$meshdir/$meshname
nu=1.1e12
tstep=6
MESHREFINE=--enable-mesh-refine
hypervis_subcycle=1
HYPERVIS_OPTS='hypervis_power=3.2  fine_ne=240 max_hypervis_courant=9991.9 qsplit=5'

name=template
nlev=20
qsize=0

echo "setting outnames"
if [ "$NPTS" == "4" ]; then
#   outnames="'area','cv_lat','cv_lon','corners','phys_lat','phys_lon','phys_cv_lat','phys_cv_lon','phys_area','hypervis'"
   outnames="'area','cv_lat','cv_lon','corners','hypervis','lat','lon','phys_lat','phys_lon'"
#   outnames="'area','hypervis'"
else
#   outnames="'area','corners','phys_lat','phys_lon','phys_cv_lat','phys_cv_lon','phys_area','hypervis'"
   outnames="'area','corners','hypervis'"
#   outnames="'area','hypervis'"
fi

echo "setting directories"

input=${HOMME_TRUNK}/test/template 
mesh=ne${ne}-${NPTS}t${tstep}l${nlev}
output=jw_baroclinic1.nc
exe=preqx
# setup the work directory

mkdir -p $wdir/preqx
mkdir -p $wdir/preqx/$name
mkdir -p $wdir/preqx/$name/movies
mkdir -p $wdir/preqx/vcoord
rsync -a ${HOMME_TRUNK}/test/vcoord/*ascii $wdir/preqx/vcoord

wdir=$wdir/preqx/$name

echo "setting build details"
cd $src
build=0
#if ( -f $exe ) build=0
make=0 # for debugging - rebuild code every run but dont re-configure

cd $wdir

rm -f $wdir/input.nl
sed s/NE/$ne/ $input/explicit20.nl.sed | sed s/TSTEP/$tstep/ |\
sed s:meshfile.\*:"mesh_file = '$meshfile'    $HYPERVIS_OPTS":   |\
sed s/qsize.\*/"qsize = $qsize"/   |\
sed s/nu=.\*/"nu=$nu"/   |\
sed s/hypervis_subcycle=.\*/"hypervis_subcycle=$hypervis_subcycle"/   |\
sed s/hypervis_subcycle_q.\*/"hypervis_subcycle_q=$hypervis_subcycle"/   |\
sed s/runtype.\*/"runtype = 0"/   |\
sed s/output_frequency.\*/"output_frequency = 1"/   |\
sed s:infilenames.\*:"infilenames='h0-tavg.nc'": |\
sed s:output_varnames1.\*:"output_varnames1=$outnames": \
> $wdir/input.nl

#sed s:output_type.\*:"output_type='pnetcdf'": \

if [ "$?" -ne "0" ]; then
	exit
fi

echo
rm -Rf $wdir/movies/jw*
srun -n 1 -c 48 $src/$exe < $wdir/input.nl | tee -a preqx.out

# the GLL interpIC template (and paraview metadata)
set echo
ncks -O -v lat,lon,corners movies/$output ${meshname}_tmp.nc
ncl $input/HOMME2META.ncl  name=\"$meshname\"  ne=$ne
rm -f ${meshname}_tmp.nc

if [ "$NPTS" == "4" ]; then
   ncks -O -v lat,lon,area,cv_lat,cv_lon movies/$output ${meshname}_tmp.nc
   ncl $input/HOMME2SCRIP.ncl  name=\"$meshname\"  ne=$ne
   rm -f ${meshname}_tmp.nc
else
   ncks -O -v lat,lon,area movies/$output ${meshname}_tmp.nc
   ncl $input/HOMME2SCRIP.ncl  name=\"$meshname\"  ne=$ne
   rm -f ${meshname}_tmp.nc
fi

if [ "${meshsub}" != "" ]; then

	# a subelement control volume interpIC template:
	set echo
	ncks -O -v phys_lat,phys_lon,phys_area movies/$output ${meshsub}_tmp.nc
	ncrename -v phys_lat,lat ${meshsub}_tmp.nc
	ncrename -v phys_lon,lon ${meshsub}_tmp.nc
	ncrename -v phys_area,area ${meshsub}_tmp.nc
	ncrename -d nphys,ncol ${meshsub}_tmp.nc
	ncl $input/HOMME2META.ncl  name=\"$meshsub\"  ne=$ne
	rm -f ${meshsub}_tmp.nc


	# subelement ESMF/SCRIP template (still need phys_area)
	ncks -O -v phys_area,phys_lat,phys_lon,phys_cv_lat,phys_cv_lon movies/$output ${meshsub}_tmp.nc
	ncrename -v phys_area,area ${meshsub}_tmp.nc
	ncrename -v phys_lat,lat ${meshsub}_tmp.nc
	ncrename -v phys_lon,lon ${meshsub}_tmp.nc
	ncrename -v phys_cv_lat,cv_lat ${meshsub}_tmp.nc
	ncrename -v phys_cv_lon,cv_lon ${meshsub}_tmp.nc
	ncl $input/HOMME2SCRIP.ncl  name=\"$meshsub\"  ne=$ne
	rm -f ${meshsub}_tmp.nc
fi

### CMZ stuff
pwd
# Correct corners in SCRIP file
ncl 'scripfile = "'$DOMAIN'.g_scrip.nc"' $input/convert_scrip_999.ncl
# Chop out lat-lon variables from template file
ncks -O -v lat,lon $DOMAIN'.g_latlon.nc' $DOMAIN'_template.nc'

mv $wdir_main/preqx/template/$DOMAIN* $wdir_main

exit 0
