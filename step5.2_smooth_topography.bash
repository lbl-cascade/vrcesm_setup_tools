#! /bin/bash
#SBATCH -N 1
#SBATCH -C haswell
#SBATCH -p regular
#SBATCH -J topogen
#SBATCH -t 00:10:00

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

source common_env.bash

module load cesm/1.4_beta05_gnu
module load nco

export OMP_STACKSIZE="256M"

SMOOTHED_TOPO_FILE=$PWD/topo/smoothed/${DOMAIN}_topo_smoothed.nc
rundir=$PWD/topo/smoothed
cfgdir=$CAMROOT/components/cam/bld

## Ensure that run directory exists
mkdir -p $rundir 

ncdata=${DOMAIN}.INIC.0000-01-01-00000.nc
bnd_topo=${DOMAIN}_topo_unsmoothed.nc
mesh_file=${DOMAIN}.g

# Link files to directories because cam namelist has character limits 
ln -sf $PWD/grid/${mesh_file} $rundir
ln -sf $PWD/topo/unsmoothed/${bnd_topo} $rundir
ln -sf $PWD/inic/${ncdata} $rundir

#########################################################
#####       VALIDATED SMOOTHING COEFFICIENTS      #######
# 8e-16 = ne120 fine, 4e-16 = ne240 fine, 3e-16 = ne384 #
#########################################################
### been using 6e-16 for ne120 stuff recently. (note from Alan Rhoades??)

smooth_phis_numcycle=12 #Number of times smoothing is applied in a CAM timestep
smooth_phis_nudt=4e-16 #Smoothing strength

#########################################################

################################################ Now let's begin CAM

## Create the namelist
pushd $rundir                      || ( echo "cd $rundir failed" && exit 1 )
$cfgdir/build-namelist -s -config $CONFIG_CACHE -ignore_ic_date -test \
                                 -csmdata $CESMDATAROOT \
                                 -namelist \
                                 "&camexp se_ftype=0 \
                                 stop_option='nsteps' \
                                 stop_n=1 \
                                 start_ymd=901 \
                                 interpolate_analysis=.true.,.false. \
                                 interp_nlat=721 \
                                 interp_nlon=1440 \
                                 restart_option='none' \
                                 inithist='NONE' \
                                 atm_adiabatic=.true. \
                                 NHTFRQ=1 \
                                 empty_htapes=.TRUE. \
                                 fincl1='PHIS_SM:I','SGH_SM:I','SGH30_SM' \
                                 fincl2='PHIS_SM:I','SGH_SM:I','SGH30_SM' \
                                 bnd_topo='$bnd_topo' \
                                 ncdata='$ncdata' \
                                 mesh_file='$mesh_file' \
                                 NDENS=2,2 \
                                 PERTLIM=0. \
                                 smooth_phis_numcycle=$smooth_phis_numcycle \
                                 smooth_phis_nudt=$smooth_phis_nudt \
                                 dtime=1 \
                                 rsplit=0 \
                                 qsplit=1 \
                                 se_nsplit=1 \
                                 nu=0 \
                                 nu_div=0 \
                                 nu_p=0 \
                                 nu_q=-1 \
                                 nu_top=2e5 \
                                 se_ne=0 \
                                 / \
                                 &dyn_se_inparm \
                                 hypervis_power=0 \
                                 hypervis_subcycle=2 \
                                 hypervis_scaling=2 \
                                 /"   #|| ( echo "build-namelist failed in $cfgdir" && exit 1 )

if [ $# -eq 0 ]; then
  echo "running CAM in $rundir"
  
  #cori haswell
  srun -n 1 -c 4 --cpu_bind=cores cam    || ( echo "CAM run failed" && exit 1 )
else
    echo "build-namelist failed in $cfgdir" && exit 1
fi

date


# generate the topography file
ncwa -O -a time camrun.cam.h1.0000-09-01-00000.nc ${SMOOTHED_TOPO_FILE}
ncrename -v PHIS_SM,PHIS ${SMOOTHED_TOPO_FILE}
ncrename -v SGH_SM,SGH ${SMOOTHED_TOPO_FILE}
ncrename -v SGH30_SM,SGH30 ${SMOOTHED_TOPO_FILE}
ncks -A -v LANDM_COSLAT,LANDFRAC $bnd_topo ${SMOOTHED_TOPO_FILE}
ncap2 -O -s 'PHIS=double(PHIS)' ${SMOOTHED_TOPO_FILE} ${SMOOTHED_TOPO_FILE}
ncap2 -O -s 'SGH30=double(SGH30)' ${SMOOTHED_TOPO_FILE} ${SMOOTHED_TOPO_FILE}
ncap2 -O -s 'SGH=double(SGH)' ${SMOOTHED_TOPO_FILE} ${SMOOTHED_TOPO_FILE}

popd

exit 0
