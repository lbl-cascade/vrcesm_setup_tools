export DOMAIN="CHANGEME"
export NUM_REFINEMENT=3
export COARSE_NE_LEVEL=30
export NUM_VERTICAL_LEVELS=30
# set this to a directory in the $SCRATCH filesystem and not /project (weird .esmf.nc permission errors happen otherwise)
export WORK_SCRATCH=$SCRATCH/$DOMAIN

# center for plotting the grid
LATBASE=37.5
LONBASE=-120.0


export VRCESM_FILES_DIR=/global/project/projectdirs/m1517/cascade/VRCESMFiles/

export CESMDATAROOT=/project/projectdirs/m1949/model_input/cesm/inputdata/

export CESM_SOURCE=/project/projectdirs/ccsm1/collections/cesm1_2_2/

# check if the datestring has already been recorded; use
# this instead so that the toolchain doesn't break when scripts
# are run across multiple days
if [ -e ".DATESTR" ]; then
    export DATESTR=`cat .DATESTR`
else
    export DATESTR=`date +%y%m%d`
    # store the datestring for later use
    echo $DATESTR > .DATESTR
fi

# load necessary modules
module use /global/common/software/m1517/users/taobrien/modulefiles/
module load squadgen
module load ncl

# make sure output directories exist
mkdir -p domains  grid  inic  maps  namelist  scrip  surfdata  topo regrid
mkdir -p $WORK_SCRATCH
