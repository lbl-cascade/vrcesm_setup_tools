#!/bin/bash

source common_env.bash

module load cube_to_target/1.2.2

# make the unsmoothed topography directory
UNSMOOTHED_TOPO_DIR=$PWD/topo/unsmoothed
UNSMOOTHED_TOPO_FILE=$UNSMOOTHED_TOPO_DIR/$DOMAIN'_topo_unsmoothed.nc'
mkdir -p $UNSMOOTHED_TOPO_DIR

# link the scrip file to the smoothing directory
ln -sf $PWD/scrip/$DOMAIN'.g_scrip.nc' $UNSMOOTHED_TOPO_DIR/target.nc
ln -sf $VRCESM_FILES_DIR/topo/USGS-topo-cube.nc \
       $UNSMOOTHED_TOPO_DIR


# run cube_to_target
pushd $UNSMOOTHED_TOPO_DIR
cube_to_target 

# change the file name
mv new-topo-file.nc $UNSMOOTHED_TOPO_FILE

# return to the original directory
popd
