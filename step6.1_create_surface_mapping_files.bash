#!/bin/bash
#SBATCH -N 4
#SBATCH -C haswell
#SBATCH -p regular
#SBATCH -t 01:00:00

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

source common_env.bash

module load esmf
module load nco

# set some environment variables for mkmapdata
export ESMFBIN_PATH=$ESMF_BINDIR
export MPIEXEC="srun -n 128 -c 2 --cpu_bind=cores"
export CSMDATA=$CESMDATAROOT

TARGET_SCRIP=$PWD/scrip/${DOMAIN}.g_scrip.nc

# set the working directory
WORKDIR=$WORK_SCRATCH/maps
mkdir -p $WORKDIR


# get clm
rsync -a $CESM_SOURCE/ $WORKDIR/cesm1_2_2/

# patch mkmapdata to avoid building grids that cause a stall
MKMAPDATAPATCH='289d288
<            "10x10min_IGBPmergeICESatGIS" \
295a295
>            #"10x10min_IGBPmergeICESatGIS" \
'
echo "$MKMAPDATAPATCH" | patch $WORKDIR/cesm1_2_2/models/lnd/clm/tools/shared/mkmapdata/mkmapdata.sh 

# generate the map files
pushd $WORKDIR/cesm1_2_2/models/lnd/clm/tools/shared/mkmapdata
echo "./mkmapdata.sh -v --batch --phys clm4_0 --gridfile ${TARGET_SCRIP} --res ${DOMAIN} --gridtype global" #&> $WORKDIR/mkmapdata.log"
./mkmapdata.sh -v --batch --phys clm4_0 --gridfile ${TARGET_SCRIP} --res ${DOMAIN} --gridtype global #&> $WORKDIR/mkmapdata.log
popd

# unstage the scratch directory
rsync -a $WORK_SCRATCH/maps/ $PWD/maps/

# link the map files to the maps directory
ln -s $PWD/maps/cesm1_2_2/models/lnd/clm/tools/shared/mkmapdata/map*.nc $PWD/maps

# link the CESM directory locally
ln -sf $PWD/maps/cesm1_2_2 $PWD/cesm1_2_2
