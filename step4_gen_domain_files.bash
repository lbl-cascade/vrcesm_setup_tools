#!/bin/bash
#
# Colin Zarzycki 9/15/2015
#
# Batch script to generate domain files for VR grids.
# OUTBASE: the low-level directory where files will be stored
# VRscripfile: Absolute path to SCRIP descriptor for VR grid
# VRname: Long VR name
# VRshort: Short VR name
#
# NOTES
# Folder will be generated at $OUTBASE/$VRname for storage
# Resultant output will be (1 each) *.lnd.* and *.ocn.* domain files
# Use tx0.1v2 mask unless using an exceptionally low-res grid (per Mariana)
#

#SBATCH -N 1
#SBATCH -C haswell
#SBATCH -p regular
#SBATCH -J gen_cesm_maps
#SBATCH -t 00:30:00

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

source common_env.bash

module load ncl
module load nco
module load cesmmappingtools
module load esmf


OUTDIR=$PWD/domains
SCRATCHBASE=$WORK_SCRATCH/domains
VRscripfile="$PWD/scrip/$DOMAIN.g_scrip.nc"
VRname=$DOMAIN
VRshort=$DOMAIN

# go to a scratch directory: note that ESMF does not appear to work on the /project filesystem
pushd $WORK_SCRATCH
mkdir -p domains

# Need to have write access to this directory, so either copy the exec
# or rebuild 
#PATH_TO_MAPPING="/global/project/projectdirs/m1517/cascade/taobrien/learning_VR_CESM/ca_stratus_test/step4/CESM_mapping_tools"
cp -r $CESMMAPPINGTOOLS $SCRATCHBASE
PATH_TO_MAPPING="$SCRATCHBASE/CESM_mapping_tools"
chmod -R 777 $PATH_TO_MAPPING

#----------------------------------------------------------------------
# Set user-defined parameters here
#----------------------------------------------------------------------
cd $PATH_TO_MAPPING/gen_mapping_files

#/global/project/projectdirs/m1517/cascade/VRCESMFiles
fileocn="$VRCESM_FILES_DIR/grids/tx0.1v2_090127.nc"
#fileocn="/glade/p/cesmdata/cseg/mapping/grids/gx1v6_090205.nc"
fileatm=${VRscripfile}
nameocn='tx0.1v2'
#nameocn='gx1v6'
nameatm=${VRname}

typeocn='global'
typeatm='global'

#----------------------------------------------------------------------
# Stuff done in a machine-specific way
#----------------------------------------------------------------------

# Determine number of processors we're running on
#host_array=($LSB_HOSTS)
#REGRID_PROC=${#host_array[@]}
REGRID_PROC=32   #only 32 will work for now

#echo $REGRID_PROC

#----------------------------------------------------------------------
# Begin general script
#----------------------------------------------------------------------

cmdargs="--fileocn $fileocn --fileatm $fileatm --nameocn $nameocn --nameatm $nameatm --typeocn $typeocn --typeatm $typeatm"
cmdargs="$cmdargs --batch --nogridcheck"
env REGRID_PROC=$REGRID_PROC ./gen_cesm_maps.sh $cmdargs
#./gen_cesm_maps.sh $cmdargs

#----------------------------------------------------------------------
# MOVING FILES
#----------------------------------------------------------------------

cdate=`date +%y%m%d`
#mkdir -p ${SCRATCHBASE}/${VRname}/mapping
#echo 'MOVING'
set +e
#mv map_*${nameatm}*${cdate}*nc ${SCRATCHBASE}/${VRname}/mapping

#----------------------------------------------------------------------
# MOVING ON TO DOMAIN
#----------------------------------------------------------------------

cd $PATH_TO_MAPPING/gen_domain_files
aaveMap=map_${nameocn}_TO_${nameatm}_aave.${cdate}.nc
./gen_domain -m $PATH_TO_MAPPING/gen_mapping_files/${aaveMap} -o tx01 -l ${VRshort}
#./gen_domain -m ${SCRATCHBASE}/${VRname}/mapping/${aaveMap} -o tx01 -l ${VRshort}
#./gen_domain -m ${SCRATCHBASE}/${VRname}/mapping/${aaveMap} -o gx1v6 -l ${VRshort}

# Move domain files to SCRATCHBASE dir
mkdir -p ${SCRATCHBASE}/${VRname}/domains
mv domain*${VRshort}*${cdate}*nc ${SCRATCHBASE}/${VRname}/domains

# Remove mapping files since they are large and we really only needed aave for domains anyway
cd $PATH_TO_MAPPING/gen_mapping_files/
rm map_*.nc

# unstage data from the scratch directory
popd
rsync -a $WORK_SCRATCH/domains/ $OUTDIR/
mv $OUTDIR/${DOMAIN}/domains/* $OUTDIR
