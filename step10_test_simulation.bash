#!/bin/bash

source common_env.bash

CESMROOT=$PWD/cesm1_2_2

# set the case name
CASENAME=test_${DOMAIN}

export VRCESMBASE=$PWD
export NCUBE=3000

# set the patch files
CAM_PATCH_FILE="$PWD/user_nl_cam.template"
CLM_PATCH_FILE="$PWD/user_nl_clm.template"

# calculate the NE level of the highest resolution
export FINE_NE_LEVEL=`python -c "print(int(${COARSE_NE_LEVEL} * 2**(${NUM_REFINEMENT}-1)))"`

# set the number of MPI tasks
NUM_MPI_TASKS=64
NUM_THREADS=1

# remove the case directory if it exists already
CASEDIR=$PWD/$CASENAME
if [ -d $CASEDIR ]; then
    rm -rf $CASEDIR
fi

# create the case
$CESMROOT/scripts/create_newcase -case ${CASENAME} -compset F_AMIP_CAM5 -res ${DOMAIN} -mach corip1

pushd $CASEDIR

#Run/build location
./xmlchange -file env_build.xml -id EXEROOT -val $PWD/bld
./xmlchange -file env_run.xml -id RUNDIR -val $PWD/run

# dynamical core
./xmlchange -file env_build.xml -id CAM_DYCORE -val "se"

# physics packages
./xmlchange -file env_build.xml -id CAM_CONFIG_OPTS -val "-phys cam5 -chem none" 

# run duration
./xmlchange -file env_run.xml -id STOP_OPTION -val "ndays"
./xmlchange -file env_run.xml -id STOP_N -val 5

# restart frequency 
./xmlchange -file env_run.xml -id REST_OPTION -val "ndays"
./xmlchange -file env_run.xml -id REST_N -val 5

#Inputdata directory
./xmlchange -file env_run.xml -id DIN_LOC_ROOT -val ${CESMDATAROOT}
./xmlchange -file env_run.xml -id DIN_LOC_ROOT_CLMFORC -val ${CESMDATAROOT}/atm/datm7

#Archive directory
./xmlchange -file env_run.xml -id DOUT_S_ROOT -val ${PWD}/archive
./xmlchange -file env_run.xml -id DOUT_S -val FALSE
./xmlchange -file env_archive.xml -id DOUT_S -val FALSE

# set MPI/OpenMP counts
COMPONENTS="ATM LND ICE OCN CPL GLC ROF WAV"
for comp in $COMPONENTS
do
    ./xmlchange -file env_mach_pes.xml -id NTASKS_${comp} -val ${NUM_MPI_TASKS}
    ./xmlchange -file env_mach_pes.xml -id NTHRDS_${comp} -val ${NUM_THREADS}
done

./cesm_setup

# add -DMESH to the compile flags
echo "CPPDEFS += -DMESH" >> Macros

# add eps_aarea to the user_nl_cpl file
echo "eps_aarea = 1e-4" >> user_nl_cpl

# generate the user_nl_c[la]m files from the templates
cat $CAM_PATCH_FILE | envsubst >> user_nl_cam
cat $CLM_PATCH_FILE | envsubst >> user_nl_clm


# build the model
./${CASENAME}.build

popd
