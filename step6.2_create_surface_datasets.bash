#!/bin/bash

source common_env.bash

module load cray-netcdf
module load PrgEnv-intel

export LIB_NETCDF=${NETCDF_DIR}/lib
export INC_NETCDF=${NETCDF_DIR}/include
export USER_LDFLAGS="-L$LIB_NETCDF -lnetcdff -lnetcdf"
export CSMDATA=$CESMDATAROOT

export WORKDIR=$PWD/surfdata

# build the mksurfdata_map executable
pushd maps/cesm1_2_2/models/lnd/clm/tools/clm4_0/mksurfdata_map/src 
make -j 8

cd ..
./mksurfdata.pl -y 1850-2000 --res usrspec --usr_gname ${DOMAIN} --usr_gdate ${DATESTR} -dinlc $CESMDATAROOT
./mksurfdata.pl -y 2000 --res usrspec --usr_gname ${DOMAIN} --usr_gdate ${DATESTR} -dinlc $CESMDATAROOT

mv surfdata*.nc $WORKDIR
mv pftdyn_hist_simyr1850-2005.txt $WORKDIR/pftdyn_${DOMAIN}_hist_simyr1850-2005.txt

popd
