#!/bin/bash 

# set the domain name and parameters
source common_env.bash

WORKDIR=$PWD/grid

pushd $WORKDIR

# generate the grid
SQuadGen \
    --refine_file $DOMAIN.png \
    --resolution $COARSE_NE_LEVEL \
    --refine_level $NUM_REFINEMENT \
    --output ${DOMAIN}.g

# generate a plot of the grid
ncl ../plot_scripts/gridplot.ncl \
            "out_type = \"png\" "\
            "latbase = $LATBASE "\
            "lonbase = $LONBASE "\
            "refine_type = \"mountain\" "\
            "coarse_ne = $COARSE_NE_LEVEL "\
            "num_refine = $NUM_REFINEMENT "\
            "num_smth = 0 "\
            "drawmtn = False"\
            "gridname = \"${DOMAIN}\" "\
            "gridfile = \"${DOMAIN}.g\""

popd
