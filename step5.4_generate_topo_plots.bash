#!/bin/bash

source common_env.bash

module load ucar_topo
module load ncl
WORKDIR=$PWD/topo/subgrid/
UCARTOPO=$WORKDIR/UCAR_Topo/

IMAGE_BASE=topo-vars-se_${DOMAIN}-gtopo30-cam_se_smooth-intermediate_ncube3000-no_anisoSGH

rm $UCARTOPO/cube_to_target/ncl/${IMAGE_BASE}.pdf

pushd $UCARTOPO
make plot
popd

convert -density 300x300 -rotate 270 $UCARTOPO/cube_to_target/ncl/${IMAGE_BASE}.pdf $PWD/topo/${IMAGE_BASE}.png
#eog $PWD/topo/${IMAGE_BASE}.png &

# generate an orthographic plot of the grid
ncl ./plot_scripts/plot_topo_ortho.ncl \
            "centerLat = $LATBASE "\
            "centerLon = $LONBASE "\
            "filename_base = \"$PWD/topo/smoothed/${DOMAIN}_topo_smoothed\""


