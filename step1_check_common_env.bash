#!/bin/bash

source common_env.bash

if [ "$DOMAIN" == "CHANGEME" ]; then
    cat << EOF
    Error: the DOMAIN variable must be changed in common_env.bash.  Please fix this.
EOF
    exit -1
fi

echo "This step does nothing; it is only to sync with step #'s in the VRCESM documentation and to avoid confusion."
