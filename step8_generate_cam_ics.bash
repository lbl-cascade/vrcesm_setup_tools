#!/bin/bash

source common_env.bash
module load tempestremap
module load nco
module load ncl

OUTDIR=$PWD/inic
WORKDIR=$WORK_SCRATCH/inic

IC_SOURCE_FILE=$CESMDATAROOT/atm/cam/inic/homme/cami_0000-01-01_ne30np4_L30_c120315.nc
IC_TARGET_FILE=${WORKDIR}/$DOMAIN.INIC.0000-01-01-00000.nc

SOURCE_MESH=$WORKDIR/ne30np4.g
TARGET_MESH=$PWD/grid/${DOMAIN}.g
OVERLAP_MESH=$WORKDIR/overlap_ne30np4_${DOMAIN}.g
WEIGHT_FILE_NON_MONOTONIC=${WORKDIR}/weights_ne30np4_${DOMAIN}_nonmonotonic.g

mkdir -p $WORKDIR
cp $OUTDIR/plot_ic.ncl $WORKDIR
pushd $WORKDIR

# generate the source mesh
GenerateCSMesh \
    --res 30 \
    --alt \
    --file $SOURCE_MESH

# generate the overlap mesh
GenerateOverlapMesh \
    --b ${SOURCE_MESH} \
    --a ${TARGET_MESH} \
    --out ${OVERLAP_MESH}

# generate regridding weights
GenerateOfflineMap \
    --in_mesh ${SOURCE_MESH} \
    --out_mesh ${TARGET_MESH} \
    --ov_mesh ${OVERLAP_MESH} \
    --in_type cgll \
    --out_type cgll \
    --in_np 4 \
    --out_np 4 \
    --out_map ${WEIGHT_FILE_NON_MONOTONIC}


ncremap -m ${WEIGHT_FILE_NON_MONOTONIC} -i ${IC_SOURCE_FILE} -o ${IC_TARGET_FILE}

ncl plot_ic.ncl \
    "source_ic = \"${IC_SOURCE_FILE}\"" \
    "target_ic = \"${IC_TARGET_FILE}\"" \

popd

rsync -a $WORKDIR/ $OUTDIR/
